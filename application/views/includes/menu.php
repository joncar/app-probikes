<div class="col-sm-3 col-xs-12">
    <div class="list-group">                    
        <a href="<?= base_url('panel/usuarios'); ?>" class="<?= !empty($menu) && $menu=='usuarios'?'active':'' ?> list-group-item">Usuarios</a>        
        <a href="<?= base_url('panel/clientes'); ?>" class="<?= !empty($menu) && $menu=='clientes'?'active':'' ?> list-group-item">Clientes</a>        
        <a href="<?= base_url('panel/vehiculos'); ?>" class="<?= !empty($menu) && $menu=='vehiculos'?'active':'' ?> list-group-item">Bicicletas</a>        
        <a href="<?= base_url('panel/grupos'); ?>" class="<?= !empty($menu) && $menu=='grupos'?'active':'' ?> list-group-item">Grupos</a>        
        <a href="<?= base_url('panel/noticias'); ?>" class="<?= !empty($menu) && $menu=='noticias'?'active':'' ?> list-group-item">Noticias</a>        
        <a href="<?= base_url('panel/alertas'); ?>" class="<?= !empty($menu) && $menu=='alertas'?'active':'' ?> list-group-item">Alertas</a>        
        <a href="<?= base_url('panel/respuestos'); ?>" class="<?= !empty($menu) && $menu=='respuestos'?'active':'' ?> list-group-item">Respuestos y/o Servicios</a>        
        <a href="<?= base_url('panel/servicios'); ?>" class="<?= !empty($menu) && $menu=='servicios'?'active':'' ?> list-group-item">Servicios realizados</a>        
        <a href="<?= base_url('panel/chat'); ?>" class="<?= !empty($menu) && $menu=='chat'?'active':'' ?> list-group-item">Chat</a>                
        <a href="<?= base_url('panel/app'); ?>" class="<?= !empty($menu) && $menu=='app'?'active':'' ?> list-group-item">Maquetador de app</a>
    </div>
</div>