<script type="text/javascript" src="http://cdn.socket.io/socket.io-1.0.3.js"></script>
<? $this->load->view('includes/nav') ?>
<section class="row" style="margin-right: 0px; margin-right:0px;">
    <?= $this->load->view('includes/menu') ?>    
    <div class="col-sm-9 col-xs-12" style="padding:20px; background:#f1fafa;">
        <link rel="stylesheet" type="text/css" href="<?= base_url('css/chat.css') ?>">
        <script src="<?= base_url('js/chat.js') ?>"></script>
        <script>WebSocketOpen(<?= $_SESSION['user'] ?>,'<?= $_SESSION['nombre'] ?>','taller');</script>
        <div class='row'>    
            <div id="chatlist" class="col-xs-8 col-sm-10">
                <div id="personChat"></div>
                <div id="list">

                </div>
            </div>
            <div id="chatperson" class="col-xs-4 col-sm-2">
                <div><b>Talleres</b></div>
                <div id="taller"></div>
                <div><b>Clientes</b></div>
                <div id="cliente"></div>
            </div>
        </div>        
        <div class="row">
            <form action="" onsubmit="return sendMessage()">
                <div class="form-group has-success has-feedback" style='margin:0px;'>                
                    <div class="input-group">                  
                      <input autocomplete="off" type="text" name="chatmessage" id="chatmessage" placeholder="Envia un mensaje" class="form-control">                  
                      <a href="javascript:sendMessage(false)" class="input-group-addon"><i class="glyphicon glyphicon-arrow-right"></i></a>
                      <label for='fileup' class="input-group-addon"><i class="glyphicon glyphicon-file"></i> <input type="file" id='fileup' style='display:none'></label>                  
                    </div>                                
                </div>
            </form>
        </div>
        <audio controls id="pitido" autoplay="">
            <source src="<?= base_url('sounds/notification.mp3') ?>" type="audio/ogg">
            <source src="<?= base_url('sounds/notification.mp3') ?>" type="audio/mpeg">
            Your browser does not support the audio element.
        </audio> 
    </div>
</section>
<img id='imgfile' style='display: none'>