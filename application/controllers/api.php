<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('rest.php');
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
class Api extends Rest {
    function posting()
    {        
        if(!empty($this->_post_args)){
            foreach($this->_post_args as $x=>$y)
                $_POST[$x] = $this->_xss_clean ($y,true);
        }
    }
    
    function login_post(){        
        $this->form_validation->set_rules('email','Email','required|valid_email');
        $this->form_validation->set_rules('password','Password','required');        
        if($this->form_validation->run())
        {
            $user = $this->db->get_where('clientes',array('email'=>$_POST['email']));
            if($user->num_rows>0){
                if($user->row()->password==md5($_POST['password']) && $user->row()->status==1)
                $this->response(array('status'=>'success','id'=>$user->row()->id,'nombre'=>$user->row()->nombre));
                elseif($user->row()->status==0)
                $this->response(array('status'=>'blocked'));
                else
                $this->response(array('status'=>'fail'));
            }
            else
            $this->response(array('status'=>'noexist'));
        }
        $this->response(array('status'=>'fail','msj'=>$this->form_validation->error_string()));
    }
    
    function registrar_post(){                
        $this->form_validation->set_rules('email','Email','required|valid_email');
        $this->form_validation->set_rules('password','Contraseña','required|min_length[8]');
        $this->form_validation->set_rules('password2','Repetir Contraseña','required|matches[password]');
        $this->form_validation->set_rules('nombre','Nombre','required');        
        $this->form_validation->set_rules('telefono','Telefono','required');
        if($this->form_validation->run())           
        {            
            $usr = $this->db->get_where('clientes',array('email'=>$_POST['email']));
            if($usr->num_rows==0){
                $data = array('email'=>$_POST['email']);
                $data['password'] = md5($_POST['password']);
                $data['nombre'] = $_POST['nombre'];
                $data['telefono'] = $_POST['telefono'];
                $data['fecha_registro'] = date("Y-m-d");
                $data['status'] = 1;
                if(!empty($_POST['direccion']))$data['direccion'] = $_POST['direccion'];
                $this->db->insert('clientes',$data);
                $this->response(array('status'=>'success','id'=>$this->db->insert_id(),'nombre'=>$_POST['nombre']));
            }
            else $this->response(array('status'=>'fail','msj'=>'El correo ingresado ya existe'));
        }
        else $this->response(array('status'=>'fail','msj'=>$this->form_validation->error_string()));
    }
    
    function actualizar_datos_post(){
        $this->form_validation->set_rules('id','ID','required');
        $this->form_validation->set_rules('email','Email','required|valid_email');
        $this->form_validation->set_rules('password','Contraseña','required|min_length[8]');
        $this->form_validation->set_rules('password2','Repetir Contraseña','required|matches[password]');
        $this->form_validation->set_rules('nombre','Nombre','required');        
        $this->form_validation->set_rules('telefono','Telefono','required');
        if($this->form_validation->run())
        {        
            $user = $this->db->get_where('clientes',array('id'=>$_POST['id']));
            $data = array('email'=>$_POST['email']);
            if($user->row()->password!=$_POST['password'])
            $data['password'] = md5($_POST['password']);
            $data['nombre'] = $_POST['nombre'];
            $data['telefono'] = $_POST['telefono'];                        
            if(!empty($_POST['direccion']))$data['direccion'] = $_POST['direccion'];
            
            $this->db->update('clientes',$data,array('id'=>$_POST['id']));
            $this->response(array('status'=>'success','id'=>$_POST['id'],'nombre'=>$_POST['nombre']));
        }
        else $this->response(array('status'=>'fail','msj'=>$this->form_validation->error_string()));
    }
    
    function bicicletas_post(){
        $this->form_validation->set_rules('id','ID','required');
        if($this->form_validation->run()){
            $email = $this->db->get_where('vehiculos',array('cliente'=>$_POST['id']));            
            $data = array();
            foreach($email->result() as $a){
                $data[$a->id] = $a;
            }
            $this->response(array('status'=>'success','datos'=>json_encode($data)));            
        }
    }
    
    function perfil_post(){
        $this->form_validation->set_rules('id','ID','required');
        if($this->form_validation->run()){
            $email = $this->db->get_where('clientes',array('id'=>$_POST['id']))->row();            
            $this->response(array('status'=>'success','datos'=>json_encode($email)));            
        }
    }
    
    function servicios_post(){
        ini_set('date.timezone', 'America/Caracas');
        date_default_timezone_set('America/Caracas');       
        $this->form_validation->set_rules('id','ID','required');
        if($this->form_validation->run()){
            $this->db->select('
                servicios.id,
                servicios.fecha_entrega,
                servicios.fecha_recibido,
                servicios.total as total_servicio,
                vehiculos.modelo as bicicleta,
                user.nombre as tecnico
            ');
            $this->db->join('vehiculos','vehiculos.id = servicios.bicicleta');
            $this->db->join('user','user.id = servicios.tecnico');
            $email = $this->db->get_where('servicios',array('servicios.cliente'=>$_POST['id']));            
            
            $data = array();
            foreach($email->result() as $e){
                $e->fecha_recibido = date("d/m/Y",strtotime($e->fecha_recibido));
                $e->fecha_entrega = date("d/m/Y",strtotime($e->fecha_entrega));
                $data[$e->id] = $e;
                $this->db->select('servicios_detalles.*, respuestos.nombre as resp');
                $this->db->join('respuestos','respuestos.id = servicios_detalles.detalle');
                $data[$e->id]->detalles = $this->db->get_where('servicios_detalles',array('servicio'=>$e->id));
                $de = array();
                foreach($data[$e->id]->detalles->result() as $d)
                    $de[$d->id] = $d;
                $data[$e->id]->detalles = $de;
            }
            //print_r($data);
            $this->response(array('status'=>'success','datos'=>json_encode($data)));            
        }
    }
    
    function alertas_post(){
        ini_set('date.timezone', 'America/Caracas');
        date_default_timezone_set('America/Caracas');       
        $this->form_validation->set_rules('id','ID','required');
        if($this->form_validation->run()){
            $this->db->select(
            '
                alertas.id,
                alertas.fecha,
                vehiculos.modelo,
                alertas.titulo,
                alertas.contenido,
                ');
            $this->db->where('fecha <= ',date("Y-m-d"));
            $this->db->where('alertas.cliente',$_POST['id']);
            $this->db->join('vehiculos','vehiculos.id = alertas.vehiculo');
            $alerta = $this->db->get('alertas');
            $data = array();
            foreach($alerta->result() as $a){
                $data[$a->id] = $a;
            }
            $this->response(array('status'=>'success','datos'=>json_encode($data)));            
        }
    }
    
    function noticias_post(){
        $this->form_validation->set_rules('id','ID','required');
        if($this->form_validation->run()){
            $email = $this->db->get_where('clientes',array('id'=>$_POST['id']))->row()->email;
            
            $this->db->like('integrantes','Global');
            $this->db->or_like('integrantes',$email);
            $grupos = $this->db->get('grupos');
                        
            $this->db->like('destinatarios','Global');
            $this->db->or_like('destinatarios',$email);
            //O por grupo
            foreach($grupos->result() as $g){
                $this->db->or_like('destinatarios',$g->nombre);
            }
            
            $this->db->order_by('fecha','ASC');
            $this->db->limit('10');
            $noticias = $this->db->get('noticias');
            if($noticias->num_rows>0){
                $data = array();
                foreach($noticias->result() as $n)
                    $data[$n->id] = $n;
                $this->response(array('status'=>'success','noticias'=>json_encode($data)));
            }
            else $this->response(array('status'=>'fail'));
        }
    }
    
    function register_gcm_post(){
        $this->form_validation->set_rules('id','ID','required');
        $this->form_validation->set_rules('gcm','GCM','required');
        if($this->form_validation->run()){
            $this->db->update('clientes',array('gcm'=>$_POST['gcm']),array('id'=>$_POST['id']));
            $this->response(array('status'=>'success'));
        }
    }
}