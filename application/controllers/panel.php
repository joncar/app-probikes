<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('main.php');
class Panel extends Main {
        
	public function __construct()
	{
		parent::__construct();
                if(empty($_SESSION['user']))
                header("Location:".base_url('index.php?redirect='.str_replace("/sgv/","",$_SERVER['REQUEST_URI'])));
                $this->load->library('html2pdf/html2pdf');
                $this->load->library('image_crud');               
	}
       
        public function index($url = 'main',$page = 0)
	{		
                $this->clientes();
	}                
        
        public function loadView($crud)
        {
            if(empty($_SESSION['user']))
            header("Location:".base_url('index.php?redirect='.str_replace("/sgv/","",$_SERVER['REQUEST_URI'])));
            else
            parent::loadView($crud);
        }
        
        function usuarios($x = '', $y = '')
        {
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('user');
            $crud->set_subject('Usuarios');               
            //Fields            
            //unsets
            $crud->unset_delete()                                  
                 ->unset_fields()
                 ->unset_columns('password');
            //Displays
            $crud->display_as('password','Contraseña');            
            //Fields types            
            //Validations  
            $crud->required_fields(
                    'nombre','email',
                    'password',
                    'status','cuenta');  
            if(empty($y))
            $crud->set_rules('email','Email','required|valid_email|is_unique[user.email]');
            $crud->set_rules('password','Contraseña','required|min_length[8]');
            $crud->field_type('password','password');
            $crud->field_type('status','true_false',array('0'=>'Bloqueado','1'=>'Activo'));
            $crud->field_type('cuenta','dropdown',array('1'=>'Admin','2'=>'Gerente','3'=>'Taller'));
            //Callbacks            
            $crud->callback_before_insert(array($this,'usuario_binsertion'));
            $crud->callback_after_insert(array($this,'usuario_ainsertion'));
            $crud->callback_before_update(array($this,'usuario_binsertion'));
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Registro de usuarios';
            $output->menu = 'usuarios';
            $this->loadView($output);
        }
        
        function clientes($x = '', $y = '')
        {
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('clientes');
            $crud->set_subject('Cliente');               
            //Fields            
            //unsets
            $crud->unset_delete()                                  
                 ->unset_fields()
                 ->unset_columns('password','gcm')
                 ->unset_fields('gcm');
            //Displays
            $crud->display_as('password','Contraseña');            
            //Fields types            
            //Validations  
            $crud->required_fields(
                    'nombre','email',
                    'password',
                    'status','direccion','telefono');  
            if(empty($y))
            $crud->set_rules('email','Email','required|valid_email|is_unique[user.email]');
            $crud->set_rules('password','Contraseña','required|min_length[8]');
            $crud->field_type('password','password');
            $crud->field_type('status','true_false',array('0'=>'Bloqueado','1'=>'Activo'));            
            //Callbacks            
            $crud->callback_before_insert(array($this,'cliente_binsertion'));            
            $crud->callback_before_update(array($this,'cliente_binsertion'));
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Registro de Clientes';
            $output->menu = 'clientes';
            $this->loadView($output);
        }
        
        function grupos($x = '', $y = '')
        {
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('grupos');
            $crud->set_subject('Grupo');               
            //Fields            
            //unsets
            $crud->unset_delete()                                  
                 ->unset_fields()
                 ->unset_columns('password');
            //Displays       
            //Fields types
            //Validations
            $data = array('Global'=>'Global');
            foreach($this->db->get('clientes')->result() as $c)
                $data[$c->email] = $c->email;
            $crud->field_type('integrantes','multiselect',$data);
            $crud->required_fields('nombre','integrantes');
            
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Registro de grupos';
            $output->menu = 'grupos';
            $this->loadView($output);
        }
        
        function vehiculos($x = '', $y = '')
        {
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('vehiculos');
            $crud->set_subject('Bicicleta');  
            $crud->set_relation('cliente','clientes','nombre');
            //Fields            
            //unsets
            $crud->unset_delete()                                  
                 ->unset_fields();
            //Displays            
            //Fields types            
            //Validations  
            $crud->required_fields(
                    'modelo','cliente');            
            //Callbacks            
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Registro de bicicletas';
            $output->menu = 'Bicicletas';
            $this->loadView($output);
        }
        
        function noticias($x = '', $y = '')
        {
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('noticias');
            $crud->set_subject('Noticias');            
            //Fields            
            //unsets
            $crud->unset_fields();
            //Displays            
            //Fields types            
            //Validations  
            $data = array('Global'=>'Global');
            foreach($this->db->get('grupos')->result() as $c)
                $data[$c->nombre] = 'Grupo: '.$c->nombre;
            foreach($this->db->get('clientes')->result() as $c)
                $data[$c->email] = $c->email;
            $crud->field_type('destinatarios','multiselect',$data);
            $crud->required_fields('titulo','contenido','destinatarios');            
            //Callbacks            
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Registro de Noticias';
            $output->menu = 'noticias';
            $this->loadView($output);
        }
        
        function alertas($x = '', $y = '')
        {
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('alertas');
            $crud->set_subject('Alerta');
            $crud->set_relation('cliente','clientes','nombre');
            $crud->set_relation('vehiculo','vehiculos','modelo');
            $crud->set_relation_dependency('vehiculo','clientes','cliente');
            //Fields            
            //unsets
            $crud->required_fields('fecha','cliente','vehiculo','titulo');            
            $crud->unset_delete()                                  
                 ->unset_fields();
            $crud->display_as('fecha','Comienza en');
            $crud->field_type('repetir','dropdown',array('0'=>'No','30'=>'Mensual','180'=>'Semestral','365'=>'Anual'));
            $crud->field_type('activo','true_false',array('0'=>'No','1'=>'Si'));
            //Displays            
            //Fields types            
            //Validations              
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Registro de Alertas';
            $output->menu = 'alertas';
            $this->loadView($output);
        }
        
        function respuestos($x = '', $y = '')
        {
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('respuestos');
            $crud->set_subject('Respuesto y/o servicio');
            $crud->field_type('tipo','dropdown',array('1'=>'Servicio','2'=>'Respuesto'));
            //Fields            
            //unsets
            $crud->required_fields('nombre','tipo','precio');
            $crud->unset_delete()
                 ->unset_fields();            
            //Displays            
            //Fields types            
            //Validations              
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Registro de respuestos y/o Servicios';
            $output->menu = 'respuestos';
            $this->loadView($output);
        }
        
        function servicios($x = '',$y = '')
        {
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('servicios');
            $crud->set_subject('Servicio');   
            $crud->set_relation('cliente','clientes','nombre');
            $crud->set_relation('bicicleta','vehiculos','modelo');
            $crud->set_relation('tecnico','user','nombre');
            
            $crud->unset_read()
                 ->unset_delete();
            
            $output = $crud->render();            
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Servicios realizados';
            $output->menu = 'servicios';
            
            if($x=='add')
                $output->output = $this->load->view('servicios',array(),TRUE);                        
            if($x=='edit' && !empty($y) && is_numeric($y)){
                $venta = $this->db->get_where('servicios',array('id'=>$y));
                $detalles = $this->db->get_where('servicios_detalles',array('servicio'=>$venta->row()->id));
                $output->output = $this->load->view('servicios',array('venta'=>$venta->row(),'detalles'=>$detalles),TRUE);                        
            }        
            $this->loadView($output);         
        }
        
        function chat(){
            $this->loadView(array('view'=>'chat','menu'=>'chat','title'=>'chat'));
        }
        
        function app()
        {            
            $this->loadView(array('view'=>'mobile','menu'=>'app'));
        }
        
        function usuario_binsertion($post,$primary = '')
        {                        
            if(!empty($primary) && $this->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'] || empty($primary))
                $post['password'] = md5($post['password']);
            return $post;
        }
        function cliente_binsertion($post,$primary = '')
        {                        
            if(!empty($primary) && $this->db->get_where('clientes',array('id'=>$primary))->row()->password!=$post['password'] || empty($primary))
                $post['password'] = md5($post['password']);
            return $post;
        }
        /*Cruds*/              
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */