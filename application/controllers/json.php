<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('panel.php');
class Json extends Panel {
        
	function __construct()
	{
		parent::__construct();
	}
        
        function getProduct($sucursal = ''){
            $this->form_validation->set_rules('codigo','Codigo','required');
            if($this->form_validation->run())
            {                
                $producto = $this->db->get_where('respuestos',array('id'=>$this->input->post('codigo')));
                if($producto->num_rows>0){
                    $pro['producto'] = $producto->row();                    
                    echo json_encode($pro);
                }
                else 
                    echo json_encode(array('producto'=>''));
                
            }
            else echo json_encode(array('producto'=>''));
        }  
        
        function getBikes(){
            $this->form_validation->set_rules('codigo','Codigo','required');
            if($this->form_validation->run())
            {                    
                $this->db->where('cliente',$_POST['codigo']);
                $producto = form_dropdown_from_query('bicicleta','vehiculos','id','modelo',0,'id="bicicleta"','',FALSE);
                echo json_encode(array('producto'=>$producto));
            }
            else echo json_encode(array('producto'=>''));
        }
        
        function ventas($edit = 'add'){
            $err = ''; 
            foreach($_POST as $p=>$v){                
                $this->form_validation->set_rules($p,$p,'required');                
            }                                    
                       
            if(isset($_POST['detalle'])){
                $x = 0;
                foreach($_POST['detalle'] as $p=>$v){
                    if(empty($v) && $p==0)$err = '<p>Se debe incluir al menos un producto</p>';
                    if($p==0 && $v==0)$x = 1;
                }
            }
            else $err .= '<p>Se debe incluir al menos un producto</p>';
            if($x==1)$err.= '<p>La cantidad de los productos deben ser mayores a 0</p>';                        
            
            if($this->form_validation->run() && empty($err))
            {
                $data = array();                
                foreach($_POST as $p=>$v){
                    if(!is_array($v))$data[$p] = $v;
                }
                $data['fecha_recibido'] = date('Y-m-d H:i:s',strtotime(str_replace("/","-",$data['fecha_recibido'])));
                $data['fecha_entrega'] = date('Y-m-d H:i:s',strtotime(str_replace("/","-",$data['fecha_entrega'])));                
                
                if($edit=='edit'){
                    $this->db->update('servicios',$data,array('id'=>$data['id']));
                    $id = $this->db->get_where('servicios',array('id'=>$data['id']))->row()->id;
                    $this->db->delete('servicios_detalles',array('servicio'=>$id));
                }
                else{
                    $this->db->insert('servicios',$data);
                    $id = $this->db->insert_id();
                }
                foreach($_POST['detalle'] as $n=>$v){
                    if(!empty($v)){
                        $data = array('servicio'=>$id);
                        $data['detalle'] = $v;
                        $data['monto'] = $_POST['monto'][$n];                        
                        $data['cantidad'] = $_POST['cantidad'][$n];
                        $data['descuento'] = $_POST['descuento'][$n];
                        $data['total'] = $_POST['total_servicio'][$n];                        
                        $this->db->insert('servicios_detalles',$data);                                                                                     
                    }
                }                
                echo json_encode(array('status'=>TRUE));
            }
            else
                echo json_encode(array('status'=>FALSE,'message'=>$this->form_validation->error_string().$err));
            
        }
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */