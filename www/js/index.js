var id = '', nombre, noticias, servicios, alertas, notificationsound;
$(document).on('ready',function(){   
   if(localStorage.id!=undefined)
    {
      document.location.href = '#main';
      id = localStorage.id;
      nombre = localStorage.nombre;
      gets();
    } 
    else document.location.href="#home";
});

function login(){
    loading('show');
    if($("#email").val()!='' && $("#password").val()!=''){
        $.post(url+'login',{email:$("#email").val(),'password':$("#password").val()},function(data){            
            if(data.status=='fail'){
                alerta("Usuario o contrase�a incorrecto");
            }
            else if(data.status=='noexist'){
                loading('close');
                document.location.href="#registro";
                $("#emailreg").val($("#email").val());
                $("#passwordreg").val($("#password").val());
            }
            else if(data.status=='blocked'){
                alerta('Usuario <b>Bloqueado</b> comuniquese con un administrador, para m�s informaci�n');
            }
            else{
                loading('close');
                save(data.nombre,data.id);
                gets();
                document.location.href="#main";
            }
        });
    }
    else
        alerta("Ingrese un usuario y una contrase�a, Si es un usuario nuevo ingrese el que desea usar para el registro");
    return false;
}

function registro(form){
    loading('show');
    $("#regbuttonsend").html('Registrarse');
    if(id==''){
        sendData(form,'registrar',function(data){
            loading('close');
            save(data.nombre,data.id); 
            gets();
            document.location.href="#main";
        },function(data){
            alerta(data.msj);
        });
    }
    else{
        sendData(form,'actualizar_datos',function(data){            
            save(data.nombre,id);
            alerta('Los datos han sido actualizados');
            gets();
            
        },function(data){
            alerta(data.msj);
        });
    }
    return false;
}

function save(nombre,id){
     id = id;
     nombre = nombre;
     localStorage.setItem("nombre",nombre);
     localStorage.setItem("id",id);
}

function unlog(){
    localStorage.removeItem('nombre');
    localStorage.removeItem('id');
    ws.close();
    document.location.href="#home";
    
    $("#nombre,#email,#password,#telefono,#direccion").val('');
}

function gets(){                    
        //$(document).ready(function(){WebSocketOpen(localStorage.id,localStorage.nombre,'cliente');})
        //Get noticias
        $.post(url+'noticias',{id:id},function(data){
            if(data.status=='success'){
                noticias = JSON.parse(data.noticias);
                fill_notices();
            }
        });       

        //Get perfil
        $.post(url+"perfil",{id:id},function(data){
            if(data.status=='success'){
                data = JSON.parse(data.datos);
                $("#emailreg").val(data.email);
                $("#passwordreg").val(data.password);
                $("#nombre").val(data.nombre);
                $("#apellido").val(data.apellido);
                $("#telefono").val(data.telefono);
                $("#direccion").val(data.direccion);
                $("#idreg").val(data.id);
                $("#regbuttonsend").html('Actualizar datos');
            }
        });
        
        //Get bicicletas
        $.post(url+"bicicletas",{id:id},function(data){
            if(data.status=='success'){               
                bicicletas = JSON.parse(data.datos);
                fill_bicicletas();
            }
        });
        
        //Get servicios
        $.post(url+"servicios",{id:id},function(data){
            if(data.status=='success'){               
                servicios = JSON.parse(data.datos);
                fill_servicios();
            }
        });
        
        //Get alertas
        $.post(url+"alertas",{id:id},function(data){
            if(data.status=='success'){               
                alertas = JSON.parse(data.datos);
                fill_alertas();
            }
        });
        
        //Notification        
        WebSocketOpen(localStorage.id,localStorage.nombre,'cliente');  
        //Register en google cloud message
        register_gcm();        
}

function fill_notices(){
    str = '<div class="list-group-item"><b>Ultimas Noticias</b></div>';
    for(i in noticias){
        str+= '<a href="javascript:show_notice(\''+i+'\')" class="list-group-item"><div>'+noticias[i].titulo+'</div><div align="right"><small style="color:lightgray">'+noticias[i].fecha+'</small></div></a>';
    }
    
    $("#noticiaslist").html(str);
}

function fill_alertas(){
    str = '<div class="list-group-item"><b>Notificaciones</b></div>';
    for(i in alertas){
        str+= '<div class="list-group-item">'+alertas[i].titulo+'</div>';
    }
    
    $("#alertaslist").html(str);
}

function fill_bicicletas(){
    str = '<div class="list-group-item"><b>Bicicletas agregadas</b></div>';
    for(i in bicicletas){
        str+= '<div class="list-group-item">'+bicicletas[i].modelo+'</div>';
    }
    
    $("#bicislist").html(str);
}

function fill_servicios(){
    str = '<div class="list-group-item"><b>Servicios realizados</b></div>';
    for(i in servicios){
        str+= '<a class="list-group-item" href="javascript:show_servicio(\''+i+'\')">'+servicios[i].fecha_recibido+': '+servicios[i].total_servicio+'</a>';
    }
    
    $("#servicioslist").html(str);
}

function show_notice(id){
    $("#titlenotice").html(noticias[id].titulo);
    $("#contenidonotice").html(noticias[id].contenido);
    document.location.href="#noticias"
}

function show_servicio(id){
    $("#bicicleta").html(servicios[id].bicicleta);
    $("#tecnico").html(servicios[id].tecnico);
    $("#fecha_recibido").html(servicios[id].fecha_recibido);
    $("#fecha_entrega").html(servicios[id].fecha_entrega);
    $("#monto_total").html(servicios[id].total_servicio);    
    
    //Servio detalle
    str = '';
    for(i in servicios[id].detalles){
        str+= '<tr><td colspan="5"><b class="label label-info">'+servicios[id].detalles[i].resp+'</b></td></tr><tr><td>'+servicios[id].detalles[i].cantidad+'</td><td>'+servicios[id].detalles[i].monto+'</td><td>'+servicios[id].detalles[i].descuento+'</td><td>'+servicios[id].detalles[i].total+'</td></tr>';
    }
    $("#servicioslistdetalle tbody").html(str);
    console.log(str);
    
    document.location.href="#showservicios";
}

// handle GCM notifications for Android
function onNotification(e) {
    switch( e.event )
    {
        case 'registered':            
            if ( e.regid.length > 0 )
            {                    
                    $.post(url+'register_gcm',{id:localStorage.id,gcm:e.regid},function(){
                        
                    });
            }
        break;
        case 'message':
            // if this flag is set, this notification happened while we were in the foreground.
            // you might want to play a sound to get the user's attention, throw up a dialog, etc.
            if (e.foreground)
            {                                            
                var soundfile = e.soundname || e.payload.sound;
                // if the notification contains a soundname, play it.
                // playing a sound also requires the org.apache.cordova.media plugin
                var my_media = new Media("/android_asset/www/"+ soundfile);
                my_media.play();
            }
            else
            {	
                    if (e.coldstart)
                            alert('--COLDSTART NOTIFICATION--');
                    else
                    alert('--BACKGROUND NOTIFICATION--');
            }            
            alert(e.payload.message);            
        break;
    }
}
function register_gcm(){
    pushNotification = window.plugins.pushNotification;    
    if ( device.platform == 'android' || device.platform == 'Android' || device.platform == "amazon-fireos" )
    {        
        pushNotification.register(
        successHandler,
        errorHandler,
        {
            "senderID":"1086544031520",
            "ecb":"onNotification"
        });
    } 
    else if ( device.platform == 'blackberry10')
    {
        pushNotification.register(
        successHandler,
        errorHandler,
        {
            invokeTargetId : "replace_with_invoke_target_id",
            appId: "replace_with_app_id",
            ppgUrl:"replace_with_ppg_url", //remove for BES pushes
            ecb: "pushNotificationHandler",
            simChangeCallback: replace_with_simChange_callback,
            pushTransportReadyCallback: replace_with_pushTransportReady_callback,
            launchApplicationOnPush: true
        });
    } 
    else 
    {
        pushNotification.register(
        tokenHandler,
        errorHandler,
        {
            "badge":"true",
            "sound":"true",
            "alert":"true",
            "ecb":"onNotificationAPN"
        });
    }
}

function successHandler (result) {
    
}

function errorHandler (error) {
    
}