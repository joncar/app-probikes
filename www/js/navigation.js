var url = 'http://74.208.184.126/probikes/api/';
var path = 'http://74.208.184.126/probikes/';
var actual_page = '#home',myScroll;
var sections = [];
$(document).on('ready',function(){
    refreshLocation(true);
    //Append alerta
    str = '<div id="alerta" class="alertainvisible">\n\
            <nav class="row">\n\
                <div class="col-xs-10">Alert</div>\n\
                <div class="col-xs-2" style="border: 1px solid white; border-radius: 1em; padding: 1px 5px; text-align:center"><a href="javascript:closealerta()">x</a></div>\n\
            </nav>\n\
            <div id="msj">\n\
                \n\
            </div>\n\
        </div>';
    $('body').append(str);    
    $('section nav').html($('body > nav').html());      
    
 });
 $(document).on('click','a',function(e){
     e.preventDefault();
     mostrar($(this).attr('href'));                
     document.location.href=$(this).attr('href');
 });
 
 $(document).on('click','aside a',function(e){
     menu();
 })
 /*$(document).on('click','body',function(e){
     if($("aside").hasClass('pagecenter'))menu();
 });*/

 function mostrar(id){
     $(id).removeClass('pageleft').addClass('pagecenter');                  
 }

 function refreshLocation(refresh){    
    activePage = document.location.href;
    activePage = activePage.split('#');
    $("body > section:not(#home)").removeClass('pagecenter').addClass('pageleft');
    if(activePage.length>1)
    {
        mostrar("#"+activePage[1]);
        actual_page = "#"+activePage[1];
        
        if(localStorage.id!=undefined && actual_page=='#home'){
         document.location.href="#main";
         actual_page = '#main';
        }
        
    }
    else {
        $("body > section:not(#home)").removeClass('pagecenter').addClass('pageleft');
         if(localStorage.id=='')
         actual_page = "#home";
         
    }
    if(refresh){
        $("body > section:not(#home)").each(function(){
            if($(this).data('async')!=undefined)
            $("#"+$(this).attr('id')+" article").load($(this).data('async'));
        })
    }
    
    
 }
 
 //Funcion para mostrar un mensaje emergente
 function alerta(msj,title){
    $("#alerta nav .col-xs-10").html(title);
    $("#alerta #msj").html(msj);
    $("#alerta").addClass('alertavisible').removeClass('alertainvisible');
}

function closealerta(){
    $("#alerta").addClass('alertainvisible').removeClass('alertavisible');
}

//Funcion para mostrar el loading emergente
function loading(param){
    if(param=='show'){
        $("#alerta nav .col-xs-10").html('Cargando...');
        $("#alerta #msj").html('<img src="img/loading.gif" style="width:100%;">');
        $("#alerta").addClass('alertavisible').removeClass('alertainvisible');
    }
    else
        $("#alerta").removeClass('alertavisible').addClass('alertainvisible');
}

//Funcion para mostrar el aside o menu lateral
function menu(param){
    if($("aside").hasClass('pageleft'))
    $("aside").removeClass('pageleft').addClass('pagecenter');
    else
    $("aside").removeClass('pagecenter').addClass('pageleft');
}
//Funcion para enviar datos al servidor
function sendData(form,uri,onsuccess,onfail){   
    formData = new FormData(form);
    $.ajax({
        url: url+uri,
        data: formData,            
        cache: false,
        contentType: false,
        processData: false,
        method:'post',
        success:function(data){            
            if(data.status=='success')
                onsuccess(data);
            else 
                onfail(data);
        }
   });
}

function back(){
    window.history.back()
}