var WebSocketServer = require('websocket').server;
var http = require('http');
var server = http.createServer();
var fs = require('fs');
server.listen(3000);
wsServer = new WebSocketServer({
    httpServer:server
});
console.log('Servidor Iniciado');

function Group(){
    this.list = [];
    
    this.add = function(cliente){
        pos = this.getPosition(cliente.id);
        if(pos==null)
        this.list.push(cliente);
        else this.list[pos] = cliente;
    }
    
    this.remove = function(cliente){
        if(cliente!=undefined){
            aux = [];
            for(i in this.list){
                if(this.list[i].id!=cliente.id)
                    aux.push(this.list[i]);
            }
            this.list = aux;
        }
    }
    
    this.update = function(cliente){
        for(i in this.list){
            if(this.list[i].id==cliente.id)
                this.list[i] = cliente;
        }
    }
    
    this.getFromId = function(id){
        cliente = null;
        for(i in this.list){
            if(this.list[i].id==id)
                cliente = this.list[i];
        }
        return cliente;
    }
    
    this.getPosition = function(id){
        cliente = null;
        for(i in this.list){
            if(this.list[i].id==id)
                cliente = i;
        }
        return cliente;
    }
    
    this.broadCast = function(cliente,evt,data,to){
        for(i in this.list){
            try{
            this.list[i].sendUTF(JSON.stringify({'evt':evt,data:data,fromId:cliente.id,fromName:cliente.nombre,to:to}));
            }catch(e){}
        }
    }
    
    this.sendMsj = function(cliente,evt,data,to){
        try{
        c = this.getFromId(to);
        c.sendUTF(JSON.stringify({'evt':evt,data:data,fromId:cliente.id,fromName:cliente.nombre,fromTipo: cliente.tipo,to:to,toTipo:c.tipo}));
        cliente.sendUTF(JSON.stringify({'evt':evt,data:data,fromId:cliente.id,fromName:cliente.nombre,fromTipo: cliente.tipo, to:to,toTipo:c.tipo}));
        }catch(e){}        
    }
}

var clientes = new Group();
var talleres = new Group();

function refreshList(){
    tal = [];
    cli = [];
    for(i in clientes.list){
        c = clientes.list[i];
        cli.push({id:c.id,nombre:c.nombre});
    }

    for(i in talleres.list){
        c = talleres.list[i];
        tal.push({id:c.id,nombre:c.nombre});
    }

    clientes.broadCast('','refreshlist',{talleres:tal,clientes:cli});
    talleres.broadCast('','refreshlist',{talleres:tal,clientes:cli});
}

wsServer.on('request',function(request){
    var cliente = request.accept(null,request.origin);    
    cliente.on('message',function(message){
        message = JSON.parse(message.utf8Data);        
        switch(message.event){
            case 'login':
                cliente.id = message.id;
                cliente.nombre = message.nombre;
                cliente.tipo = message.tipo;
                cliente.file = [];
                switch(message.tipo){
                    case 'taller': talleres.add(cliente); break;
                    case 'cliente': clientes.add(cliente); break;
                }
                refreshList();
            break;
            case 'message':
                cliente.sendMsj(message);
            break;
            
            case 'sendfile': 
                cliente.file.push(message.data);
            break;
            
            case 'sendfilefinish':
                cliente.sendFile(message);
            break;
        }
        //console.log(message);
    }); 
    
    cliente.sendMsj = function(message){
        t = cliente.tipo=='taller'?talleres:clientes;
        switch(message.tipo){
            case 'taller': 
                talleres.sendMsj(cliente,'message',message.data,message.to);
            break;
            case 'cliente': 
                clientes.sendMsj(cliente,'message',message.data,message.to);                
            break;
            case 'Global': 
                clientes.broadCast(cliente,'message',message.data,'Global');
                talleres.broadCast(cliente,'message',message.data,'Global');
            break;
        }
    }
    
    cliente.sendFile = function(message){
        
        t = cliente.tipo=='taller'?talleres:clientes;
        str = '';
        for (i in cliente.file)
            str+= cliente.file[i];
        console.log(cliente.file.length);        
        message.data = str; 
        console.log(str.length);
        switch(message.tipo){
            case 'taller':
                for(i in cliente.file)
                talleres.sendMsj(cliente,'sendFile',cliente.file[i],message.to);
                talleres.sendMsj(cliente,'sendFileFinish','',message.to);
            break;
            case 'cliente': 
                for(i in cliente.file)
                clientes.sendMsj(cliente,'sendFile',cliente.file[i],message.to);                
                clientes.sendMsj(cliente,'sendFileFinish','',message.to);                
            break;
            case 'Global': 
                clientes.broadCast(cliente,'sendFile',message.data,'Global');
                talleres.broadCast(cliente,'sendFile',message.data,'Global');
            break;
        }
        cliente.file = [];
    }
    
    cliente.on('close',function(reasonCode, description){
        switch(cliente.tipo){
            case 'taller': 
                talleres.remove(cliente);
            break;
            case 'cliente': 
                clientes.remove(cliente);
            break;
        }
        
        refreshList();
    });
})