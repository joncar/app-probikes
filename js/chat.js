var ws = null;
var toTip = 'Global';
var to = '';
var id = '';
var tipo = '';
var messages = [];
var filefromserver = [];
function WebSocketOpen(userid,nombre,tip)
{                        
    $("#chatlist #list").append('<div>Conectando con el servidor...</div>');
    ws = io.connect('http://74.208.184.126:3000');
    ws.on('connect',function(){
        ws.on('onopen', function() {
            $("#chatlist #list").html('');
            $("#chatlist #personChat").html('Chateando con: <span class="label label-info">Global</span>');                    
            userid = userid==undefined?localStorage.id:userid;
            nombre = nombre==undefined?localStorage.nombre:nombre;
            tip = tip==undefined?'cliente':tip;
            ws.send(JSON.stringify({event:'login',id:userid,nombre:nombre,tipo:tip}));  
            id = userid;
            tipo = tip;
        });

        ws.on('onmessage',function(evt){        
             data = JSON.parse(evt);           
                switch(data.evt){
                    case 'refreshlist': 
                        refreshlist(data.data);
                    break;
                    case 'message': 
                        inputMessage(data);
                        messages.push(data);
                    break;
                    case 'sendFile':
                        filefromserver.push(data.data);                   
                    break;
                    case 'sendFileFinish':
                        console.log(filefromserver.length);                   
                        str = '';
                        for(i in filefromserver){
                            str+=filefromserver[i];
                        }
                        filefromserver = [];
                        console.log(str.length);
                        data.data = '<img src="'+str+'" style="width:20%">';
                        inputMessage(data);
                        messages.push(data);
                    break;
                }
                console.log(evt);           
        });

        ws.on('onclose', function() {
             $("#chatlist #list").append('<div class="alert alert-danger">No se ha podido conectar con el servidor de chat...</div>');
        });

        ws.send = function(msj){
            ws.emit('message',msj);
        }

        ws.SendMsj = function(msj){          
             if(msj!='')
             ws.send(JSON.stringify({event:'message',data:msj,to:to,tipo:toTip}));  
         };

         ws.SendFile = function(file){
             
             fragmentos = parseInt(file.length/8192)+1;
            console.log(fragmentos);
            console.log(file.length);
            for(i=0;i<fragmentos;i++)
            {
                fromInitFile = i*8192;
                toFinishFile = (i+1)*8192;
                str = file.substring(fromInitFile,toFinishFile);
                ws.send(JSON.stringify({event:'sendfile',data:str,to:to,tipo:toTip}));
            }           
            ws.send(JSON.stringify({event:'sendfilefinish',data:'',to:to,tipo:toTip}));
         }
       });
       /*ws = new WebSocket("http://74.208.184.126:3000");
       console.log(ws);
       ws.onopen = function()
       {
          $("#chatlist #list").html('');
          $("#chatlist #personChat").html('Chateando con: <span class="label label-info">Global</span>');          
          ws.send(JSON.stringify({event:'login',id:userid,nombre:nombre,tipo:tip}));  
          id = userid;
          tipo = tip;
       };
       ws.onmessage = function (evt) 
       {        
           data = JSON.parse(evt.data);           
           switch(data.evt){
               case 'refreshlist': 
                   refreshlist(data.data);
               break;
               case 'message': 
                   inputMessage(data);
                   messages.push(data);
               break;
               case 'sendFile':
                   filefromserver.push(data.data);                   
               break;
               case 'sendFileFinish':
                   console.log(filefromserver.length);                   
                   str = '';
                   for(i in filefromserver){
                       str+=filefromserver[i];
                   }
                   filefromserver = [];
                   console.log(str.length);
                   data.data = '<img src="'+str+'" style="width:20%">';
                   inputMessage(data);
                   messages.push(data);
               break;
           }
           console.log(data);

       };                 
       ws.SendMsj = function(msj){          
          if(msj!='')
          ws.send(JSON.stringify({event:'message',data:msj,to:to,tipo:toTip}));  
       };
       
       ws.SendFile = function(file){
           fragmentos = parseInt(file.length/8192)+1;
           console.log(fragmentos);
           console.log(file.length);
           for(i=0;i<fragmentos;i++)
           {
               fromInitFile = i*8192;
               toFinishFile = (i+1)*8192;
               str = file.substring(fromInitFile,toFinishFile);
               ws.send(JSON.stringify({event:'sendfile',data:str,to:to,tipo:toTip}));
           }           
           ws.send(JSON.stringify({event:'sendfilefinish',data:'',to:to,tipo:toTip}));
       }
       
       ws.onclose = function()
       { 
           $("#chatlist #list").append('<div class="alert alert-danger">No se ha podido conectar con el servidor de chat...</div>');
       };*/
}

function sendMessage(){
    ws.SendMsj($("#chatmessage").val());
    $("#chatmessage").val('');
    return false;
}

function refreshlist(data){
    $("#chatperson #taller").html('<ul></ul>');
    $("#chatperson #cliente").html('<ul></ul>');
    for(i in data.talleres){
        d = data.talleres[i];
        $('#taller ul').append('<li id="chat_'+d.id+'"><a href="javascript:seleccionar_chat(\''+d.id+'\',\''+d.nombre+'\',\'taller\')"><i class="glyphicon glyphicon-user"></i> '+d.nombre+'</a></li>');
    }
    for(i in data.clientes){
        d = data.clientes[i];
        $('#cliente ul').append('<li id="chat_'+d.id+'"><a href="javascript:seleccionar_chat(\''+d.id+'\',\''+d.nombre+'\',\'cliente\')"><i class="glyphicon glyphicon-user"></i> '+d.nombre+'</a></li>');
    }
}

function inputMessage(data){
    a = data.fromId==id && data.fromTipo==tipo?'success':'info';
    if(data.fromId==id || data.to==id && to==data.fromId || data.to=='Global' && toTip=='Global')
    $("#chatlist #list").append('<div class="alert alert-'+a+'"><b>'+data.fromName+'</b>: '+data.data+'</div>');
    else{
        $("#"+data.fromTipo+" #chat_"+data.fromId).addClass('activechat');
    }
}

function seleccionar_chat(ids,person,tipo){
    toTip = tipo;
    to = ids;
    
    getMessages();
    $("#"+tipo+" #chat_"+ids).removeClass('activechat');
    if(ids=='')
        $("#chatlist #personChat span").html('Global');
    else{
        $("#chatlist #personChat span").html(person+" <a href='javascript:seleccionar_chat(\"\",\"Vacio\",\"Global\")'><i class='glyphicon glyphicon-remove'></i></a>");        
    }
}

function getMessages(){
    $("#chatlist #list").html('');    
    for(i in messages)
    {
        m = messages[i];
        console.log(to);
        x = false;
        if(m.to=='Global' && to=='') //Si es chat global
            x = true;
        if(m.to!='Global' && to!=''){
            if(m.to==id && m.toTipo==tipo && m.fromTipo==toTip && m.fromId==to) //Si me esta llegando
                x = true;
            else if(m.to==to && m.toTipo==toTip) //Si estoy enviando
                x = true;
        }
        if(x){
            a = m.fromId==id && m.fromTipo==tipo?'success':'info';
            $("#chatlist #list").append('<div class="alert alert-'+a+'"><b>'+m.fromName+'</b>: '+m.data+'</div>');
        }
    }
}


function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imgfile').attr('src', e.target.result);
            uploadFile();
        }

        reader.readAsDataURL(input.files[0]);
    }
}

//Transformar la foto para enviarla
function getBase64Image(img) {
  // Create an empty canvas element
  var canvas = document.createElement("canvas");
  canvas.width = img.width;
  canvas.height = img.height;
 
  // Copy the image contents to the canvas
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0);
 
  // Get the data-URL formatted image
  // Firefox supports PNG and JPEG. You could check img.src to guess the
  // original format, but be aware the using "image/jpg" will re-encode the image.
  var dataURL = canvas.toDataURL("image/png");
  return dataURL;
}

function uploadFile(){
    ws.SendFile($("#imgfile").attr('src'));
    
}

$(document).ready(function(){
    $(document).on('change','#fileup',function(){
        readURL(this);        
    })
})